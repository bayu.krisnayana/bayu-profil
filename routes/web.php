<?php
use Illuminate\Support\Facades\Route;
// use App\Models\index;
// use App\Models\about;
// use App\Models\kontak;
// use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\HomePageController;
// use App\Http\Controllers\AboutPageController;
// use App\Http\Controllers\KontakPageController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/index', [App\Http\Controllers\IndexPageController::class, 'index']);
Route::get('/about', [App\Http\Controllers\AboutPageController::class, 'about']);
Route::get('/contact',[App\Http\Controllers\ContactPageController::class, 'contact']);
Route::get('/layout',[App\Http\Controllers\LayoutPageController::class, 'layout']);
Route::get('/portfolio',[App\Http\Controllers\PortfolioPageController::class, 'portfolio']);
Route::get('/welcome',[App\Http\Controllers\WelcomePageController::class, 'welcome']);
Route::get('/download',[App\Http\Controllers\WelcomePageController::class, 'download']);